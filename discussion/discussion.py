# Mini Exercise 1
students = ["Kim Minjeong", "Yu Jimin", "Bae Joohyun", "Son Seungwan", "Kang Seulgi"]
grades = [1.1, 1.2, 1.0, 1.7, 1.45]

for student, grade in zip(students, grades):
    print(f"The grade of {student} is {grade}")

# Mini Exercise 2
car = {
    "brand": "Tesla",
    "model": "Model S",
    "year_of_make": "1459",
    "color": "Red"
}

print(f"I own a {car['brand']} {car['model']} and it was made in {car['year_of_make']}")