class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
    
    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} program.")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}")


name = input("Camper Name: ")
batch = input("Camper Batch: ")
course = input("Camper Course: ")

zuitt_camper = Camper(name, batch, course)
zuitt_camper.info()
zuitt_camper.career_track()